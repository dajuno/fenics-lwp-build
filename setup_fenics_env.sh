#!/bin/bash
source env_build.sh

pew in fenics-${TAG} pew add ${PREFIX}/lib/python${PYTHON_VERSION}/site-packages
pew in fenics-${TAG} pew add ${PREFIX}/lib64/python${PYTHON_VERSION}/site-packages

mkdir -p ${PREFIX}/bin

# Copy fenics environment for intel modules to fenics-`tag`/bin
if [ -f "${PREFIX}/bin/env_fenics_run.sh" ]; then
    cp ${PREFIX}/bin/env_fenics_run.sh ${PREFIX}/bin/env_fenics_run.sh.bak
fi
if [ -f "${PREFIX}/bin/env_build.sh" ]; then
    cp ${PREFIX}/bin/env_build.sh ${PREFIX}/bin/env_build.sh.bak
fi
cp env_build.sh ${PREFIX}/bin

echo "# source file of the fenics-${TAG} environment
# source this file, then run  pew workon fenics-${TAG}

export CC=gcc
export CXX=c++
export FC=gfortran
# export F77=gfortran
# export F90=gfortran

export MPICC=mpicc
export MPICXX=mpicxx
export MPIF77=mpifort
export MPIF90=mpifort
export MPIEXEC=mpiexec

export PREFIX=${PREFIX}

unset PYTHONPATH

export PATH=\${PREFIX}/bin:\${PATH}
export LD_LIBRARY_PATH=\${PREFIX}/lib:\${PREFIX}/lib64:\${LD_LIBRARY_PATH}
# export PYTHONPATH=\${PREFIX}/lib/python${PYTHON_VERSION}/site-packages:\${PYTHONPATH}
export MANPATH=\${PREFIX}/share/man:\${MANPATH}
export PKG_CONFIG_PATH=\${PREFIX}/lib/pkgconfig:\${PKG_CONFIG_PATH}

export PETSC_DIR=\${PREFIX}
export SLEPC_DIR=\${PREFIX}

# export INSTANT_SYSTEM_CALL_METHOD=\"COMMANDS\"      # this fixes subprocess.py error on cluster
# export INSTANT_CACHE_DIR=\${PREFIX}/cache/instant
# export INSTANT_ERROR_DIR=\${PREFIX}/cache/instant" \
    > ${PREFIX}/bin/env_fenics_run.sh

chmod +x ${PREFIX}/bin/env_fenics_run.sh



echo "
Load FEniCS ${TAG} environment with 
    source ${PREFIX}/bin/env_fenics_run.sh

Load python virtualenv with
    pew workon fenics-${TAG}"
