# fenics-wias-build #
### Scripts for building FEniCS on the WIAS cluster ###
_forked from [fenics-gaia-cluster](https://bitbucket.org/unilucompmech/fenics-gaia-cluster)_

This collection of scripts will automatically build current releases of
[FEniCS](http://fenicsproject.org) with PETSc and all dependencies on the WIAS computers. As of September 2017, these are by default:

- CMake 3.9.3
- MPICH 3.2
- Boost 1.65.1
- OpenBLAS 0.2.20
- HDF5 1.8.20
- Eigen 3.3.4
- zlib 1.2.11
- szip 2.1.1
- petsc 3.8.3
- slepc 3.8.2
- swig 3.0.12
- doxygen 1.8.12
- numpy, scipy, ... (latest)
- dolfin 2017.2.0.post0
- fenics python modules (FFC, FIAT, ...) 2017.2.0(.post0)


## Prerequisites ##

### python 3 ###
Python 3 with [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs) required.
This build uses `python 3.4.5` already installed on the `erhard-xx` machines. All FEniCS related python modules will be installed within an isolated virtual environment, in order to avoid conflicts with the system modules.
In particular, if you want to install different builds of FEniCS (different versions, packages (e.g., OpenMPI vs. MPICH, Intel MKL vs. OpenBLAS), or for different processor micro-architectures, see below), this will be helpful.

Here we use [pew](https://github.com/berdario/pew) to manage the virtualenvs.

Install pew locally using pip:
```
$ pip3 install pew --user
```
The `--user` option installs into `~/.local`.
If different python versions are to be used, for example:
```shell
$ pip3 install pew --prefix=$HOME/python3.5_intel2016.2.0
```
Add `$HOME/.local/bin` or `<prefix>/bin` to the search path in your `.bashrc`:

```shell
export PATH=$HOME/.local/bin:$PATH
```

Note: pew will add a comment to your `.bashrc` which will produce (harmless) errors on the login node. In order to avoid this replace the pew related line by

```bash
if [[ $HOST == "erhard-"* ]]; then
    source $(pew shell_config)
fi
```

## Compiling instructions ##

First clone this repository, for example to the location `$HOME/dev`.

```shell
$ mkdir -p $HOME/dev && cd $HOME/dev
$ git clone git@bitbucket.org:dajuno/fenics-wias-build.git
```

### SETUP ###

The main file is `build_all.sh`. Modify it to set the FEniCS version to be used. The `$TAG` variable (can be changed) specifies the directories FEniCS and its dependencies are installed to and the name of the virtual environment.
It is recommended to set `continue_on_key=true` for the first build in order to check that each dependency was installed correctly!

The script calls a number of `build_xxx.sh` scripts which download and install the corresponding applications. Edit these files to change the version, compile flags, etc.
The calls can be commented if it was already built, e.g., if a number of programs was built correctly until an error occurred in, say, `build_dolfin.sh`, because of a network timeout.

Note that if you want to rebuild everything the `$PREFIX` and the `$BUILD_DIR` dirs should be deleted, also the python virtual environment with `pew rm $TAG`.


**NOTE**: The WIAS computers `erhard-01` - `erhard-21` use different processor micro-architectures.
The modules need to be compiled for each architecture you want to use FEniCS on, see the table below.

For example, if you want to use FEniCS on nodes using Westmere and Haswell CPUs, it is recommended to create a complete build, e.g., `TAG=2017.1.0.post0-westmere`, on one of the Westmere (erhard-i80-1024) machines, including the python virtual environment.
Then change the `$TAG` variable to `2017.1.0.post0-haswell` and run `./build_all.sh` on a Haswell computer. Now you have two separate builds of FEniCS, each of which will only run on the selected CPU type.

List of CPUs of the `erhard-xx` computers:

| number   | group(1)   | CPU          | arch           |
| -------- | ---------- | ------------ | -------------- |
| 1-3      | i80-1024   | E7-4870      | Westmere       |
| 4-5      | i64-512    | E5-4650      | Sandy Bridge   |
| 6-9      | i32-128    | E5-2680      | Sandy Bridge   |
| 10-17    | i56-256    | E5-2697 v3   | Haswell        |
| 18-21    | i64-512    | E5-2698 v3   | Haswell        |

(1): group code is i"number of cores"-"RAM"

Make sure the environments are correctly set in `env_build.sh` and `setup_env_fenics.env`. Also revise the individual build files.

### INSTALL ###

In order to build FEniCS run 
```shell
$ ./build_all.sh |& tee -a build.log
```
on the compute node inside the `fenics-wias-build` directory.

Wait for the build to finish. The output of
the build will be stored in `build.log` as well as printed on the screen.

## Running FEniCS ##
To activate a FEniCS build you need to source the environment file created by the build script and activate the virtualenv. The corresponding lines are printed after building is completed.
```shell
$ source <prefix>/bin/env_fenics_run.sh
$ pew workon fenics-<tag>
```
Now you can run python/ipython. `import dolfin` should work. Try running `python poisson.py` and `mpirun -n 4 python poisson.py`.

## Troubleshooting ##

- If an error occurs, check that the environment and the virtualenv have been correctly loaded, e.g, with `which python`, `pip show dolfin`, `pip show petsc4py` which should point to the correct versions.
- Check in the `build.log` if all dependencies were built correctly, in particular PETSc and DOLFIN. An exhaustive summary of the DOLFIN configuration is printed and should be checked.
- If python crashes with "Illegal Construction" upon `import dolfin`, one of the dependencies was probably built on a different architecture. Make sure, e.g., petsc4py is picked up from the correct location and pip did not use a cached version when installing it!
- A common error is that the`$PYTHONPATH` variable conflicts with the virtual environment, when the wrong python modules are found. To that end, in `env_build.sh` and `env_fenics_run.sh`, this variable is `unset`. Make sure it is not modified afterwards.
