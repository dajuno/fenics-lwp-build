#!/bin/bash
source env_build.sh

# mkdir -p $BUILD_DIR/tar
# mkdir -p ${PREFIX}/lib/python2.7/site-packages/

if [[ $FENICS_VERSION == 2017.2.0* ]]; then
    PYPI_FENICS_VERSION=">=2017.2.0,<2018.1.0"
    pew in fenics-${TAG} pip install fenics${PYPI_FENICS_VERSION}
elif [[ $FENICS_VERSION > "2017.1.0" ]]; then
    # this should work for >= 2017.1.0.post0
    pew in fenics-${TAG} pip install fenics==${FENICS_VERSION}
else

    # FIAT
    cd $BUILD_DIR && \
        wget --quiet --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/fiat/downloads/fiat-${FENICS_VERSION}.tar.gz && \
        tar -xf tar/fiat-${FENICS_VERSION}.tar.gz && \
        cd fiat-${FENICS_VERSION} && \
        pew in fenics-${TAG} python setup.py install

    pew in fenics-${TAG} pip install ffc==${FENICS_VERSION} \
        dijitso==${FENICS_VERSION} \
        instant==${FENICS_VERSION} \
        ufl==${FENICS_VERSION}

fi


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

# # INSTANT
# cd $BUILD_DIR && \
#     wget --quiet --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/instant/downloads/instant-${FENICS_VERSION}.tar.gz && \
#     tar -xf tar/instant-${FENICS_VERSION}.tar.gz && \
#     cd instant-${FENICS_VERSION} && \
#     pew in fenics-${TAG} python setup.py install

# if [ "$continue_on_key" = true ]; then
#     echo "Press any key to continue..."
#     read -n 1
# fi

# # UFL
# cd $BUILD_DIR && \
#     wget --quiet --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/ufl/downloads/ufl-${FENICS_VERSION}.tar.gz && \
#     tar -xf tar/ufl-${FENICS_VERSION}.tar.gz && \
#     cd ufl-${FENICS_VERSION} && \
#     pew in fenics-${TAG} python setup.py install

# if [ "$continue_on_key" = true ]; then
#     echo "Press any key to continue..."
#     read -n 1
# fi

# # DIJITSO
# cd $BUILD_DIR && \
#    wget --quiet --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/dijitso/downloads/dijitso-${FENICS_VERSION}.tar.gz && \
#    tar -xf tar/dijitso-${FENICS_VERSION}.tar.gz && \
#    cd dijitso-${FENICS_VERSION} && \
#    pew in fenics-${TAG} python setup.py install

# if [ "$continue_on_key" = true ]; then
#     echo "Press any key to continue..."
#     read -n 1
# fi

# # FFC
# cd $BUILD_DIR && \
#     wget --quiet --read-timeout=10 -nc -P tar https://bitbucket.org/fenics-project/ffc/downloads/ffc-${FENICS_VERSION}.tar.gz && \
#     tar -xf tar/ffc-${FENICS_VERSION}.tar.gz && \
#     cd ffc-${FENICS_VERSION} && \
#     pew in fenics-${TAG} python setup.py install

# if [ "$continue_on_key" = true ]; then
#     echo "Press any key to continue..."
#     read -n 1
# fi

