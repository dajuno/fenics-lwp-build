#!/bin/bash
source env_build.sh

ZLIB_VERSION="1.2.11"

echo "Downloading and building ZLIB ${ZLIB_VERSION}"

mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --quiet --read-timeout=10 -nc -P tar http://zlib.net/zlib-${ZLIB_VERSION}.tar.gz && \
   tar -xf tar/zlib-${ZLIB_VERSION}.tar.gz && \
   cd zlib-${ZLIB_VERSION} && \
   ./configure --prefix=${PREFIX} && \
   make && \
   make install


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
