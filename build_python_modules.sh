#!/bin/bash
source env_build.sh

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

MPI4PY_VERSION=3.0.0
PETSC4PY_VERSION=3.8.1 
SLEPC4PY_VERSION=3.8.0 

pew in fenics-${TAG} pip3 install -I numpy scipy  && \
pew in fenics-${TAG} \
    pip3 install -I --no-cache-dir https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-${MPI4PY_VERSION}.tar.gz && \
pew in fenics-${TAG} \
    pip3 install -I --no-cache-dir https://bitbucket.org/petsc/petsc4py/downloads/petsc4py-${PETSC4PY_VERSION}.tar.gz && \
pew in fenics-${TAG} \
    pip3 install -I --no-cache-dir https://bitbucket.org/slepc/slepc4py/downloads/slepc4py-${SLEPC4PY_VERSION}.tar.gz && \
pew in fenics-${TAG} pip3 install -I pyyaml sympy ipython

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
