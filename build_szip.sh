#!/bin/bash
source env_build.sh

SZIP_VERSION="2.1.1"

echo "Downloading and building SZIP ${SZIP_VERSION}"

mkdir -p $BUILD_DIR/tar

#   wget --quiet --read-timeout=10 --no-check-certificate -nc -P tar https://support.hdfgroup.org/ftp/lib-external/szip/${SZIP_VERSION}/src/szip-${SZIP_VERSION}.tar.gz && \
# use curl instead of wget quiet because of openSSL errors
cd ${BUILD_DIR} && \
   curl --connect-timeout 10 -o tar/szip-${SZIP_VERSION}.tar.gz https://support.hdfgroup.org/ftp/lib-external/szip/${SZIP_VERSION}/src/szip-${SZIP_VERSION}.tar.gz && \
   tar -xf tar/szip-${SZIP_VERSION}.tar.gz && \
   cd szip-${SZIP_VERSION} && \
   ./configure --prefix=${PREFIX} && \
   make && \
   make install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
