# source this in your shell

echo "setting build environment"

export CC=gcc
export CXX=c++
export FC=gfortran
# export F77=gfortran
# export F90=gfortran

export MPICC=mpicc
export MPICXX=mpicxx
export MPIF77=mpifort
export MPIF90=mpifort
export MPIEXEC=mpiexec

unset PYTHONPATH

if [ ! -z "${PREFIX}" ]; then
    export PATH=${PREFIX}/bin:${PATH}
    export LD_LIBRARY_PATH=${PREFIX}/lib:${PREFIX}/lib64:${LD_LIBRARY_PATH}
    export INCLUDE_PATH=${PREFIX}/include:${INCLUDE_PATH}
#    export PYTHONPATH=${PREFIX}/lib/python${PYTHON_VERSION}/site-packages:${PYTHONPATH}
fi
